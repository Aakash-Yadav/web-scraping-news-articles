# web scraping news articles

Web Scraping News Articles and Tweets From Static And Dynamic Web Pages Using Python



## Problem Statement
The purpose of this project is to use various web scraping techniques to scrape tweets from Twitter and news articles from a static and a dynamic news website.


### Beautiful Soup

Beautiful Soup is a Python library for pulling data out of HTML and XML files. It works with your favorite parser to provide idiomatic ways of navigating, searching, and modifying the parse tree. It commonly saves programmers hours or days of work.

These instructions illustrate all major features of Beautiful Soup 4, with examples. I show you what the library is good for, how it works, how to use it, how to make it do what you want, and what to do when it violates your expectations.

### Requests: 
Requests III allows you to send organic, grass-fed HTTP/1.1 & HTTP/2 (wip) requests, without the need for manual thought-labor. There’s no need to add query strings to your URLs, or to form-encode your POST data. Keep-alive and HTTP connection pooling are 100% automatic, as well.


##### Using bs4 and requests scrape data from news articles:

![alt text](https://xp.io/storage/1PtWOQxK.png)

##### latest news in csv file 
![alt text](https://xp.io/storage/1PtZruTE.png)
