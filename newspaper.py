from bs4 import BeautifulSoup 
from requests import get 
import csv
'Append data From Websit in to Csv'
'BeautifulSoup MAIN'
def Store_BeautifulSoup(n):
    'Get data From Link'
    Soup = BeautifulSoup(get(f'https://indianexpress.com/section/india/page/{n}/').text,'lxml')
#     <div class="articles first">
#             href="https://indianexpress.com/article/india/past-govts-never-fulfilled-promises-we-will-uttarakhand-cm-7487690/">Past
#             govts never fulfilled promises, we will: Uttarakhand CM</a>
#     </h2>
#     <div class="date">September 3, 2021 10:31:32 pm</div>
#     <p>Pushkar Singh Dhami, who completes two months as CM on Saturday, said more than 120 projects have been completed
#         in his 60 days in power.
#     </p>
# </div>
    csv_file = open(f'Today-News page {n}.csv','w')
    csv_writer = csv.writer(csv_file)
    csv_writer.writerow(['Date','Headline','Summary'])
    for News in Soup.find_all('div',class_ = 'articles'):
        Date = News.find('div',class_='date').text
        Head_line = News.h2.text
        Summary = News.p.text 
        'Append Data In Csv'
        csv_writer.writerow([Date,Head_line,Summary])
    print(f'Page {n}')
    csv_file.close()

if __name__ == '__main__':
    data = [x for x in range(1,10)]
    list(map(Store_BeautifulSoup,data))
   

